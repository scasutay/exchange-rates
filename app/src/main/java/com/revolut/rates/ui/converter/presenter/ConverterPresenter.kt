package com.revolut.rates.ui.converter.presenter

import com.revolut.rates.base.mvp.BasePresenter
import com.revolut.rates.base.util.SchedulerProvider
import com.revolut.rates.rest.AppService
import com.revolut.rates.rest.model.CurrencyRate
import com.revolut.rates.rest.model.RateResponse
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by servetcanasutay on 02/10/2018
 */

class ConverterPresenter @Inject constructor(var api: AppService, disposable: CompositeDisposable, scheduler: SchedulerProvider) : BasePresenter<ConverterView>(disposable, scheduler) {

    private companion object {
        private const val DEFAULT_BASE_CURRENCY = "EUR"
    }

    private var rateList: ArrayList<CurrencyRate> = ArrayList()
    private var currentBaseCurrency = ""
    private var amount = 1.0

    fun getRates(currencyRates: ArrayList<CurrencyRate>) {
        this.rateList = ArrayList(currencyRates)
        val newBaseCurrency: String

        if (currencyRates.isNotEmpty()) {
            amount = currencyRates[0].currencyRate
            newBaseCurrency = currencyRates[0].currencyCode
        } else {
            amount = 1.0
            newBaseCurrency = DEFAULT_BASE_CURRENCY
        }

        if (newBaseCurrency != currentBaseCurrency) {
            currentBaseCurrency = newBaseCurrency
            disposable.clear()

            disposable.add(api.getLatest(currentBaseCurrency)
                    .subscribeOn(scheduler.io())
                    .observeOn(scheduler.ui())
                    .repeatWhen { completed -> completed.delay(1, java.util.concurrent.TimeUnit.SECONDS) }
                    .subscribe({ result -> onResponse(result) }, {}))
        }
    }

    private fun onResponse(rateResponse: RateResponse) {
        with(rateList) {
            if (size > 0) {
                rateResponse.rates[rateResponse.base] = 1.0
                rateList = ArrayList(map { CurrencyRate(it.currencyCode, rateResponse.rates[it.currencyCode]!! * amount) })
            } else {
                add(CurrencyRate(currentBaseCurrency, amount))
                addAll(rateResponse.rates.map { CurrencyRate(it.key, it.value * amount) })
            }
            view?.onCurrenciesFetched(this)
        }

    }
}