package com.revolut.rates.ui.converter.adapter;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.revolut.rates.rest.model.CurrencyRate;

import java.util.ArrayList;

/**
 * Created by servetcanasutay on 08/10/2018
 */
public class ConverterDiffCallback extends DiffUtil.Callback {

    private ArrayList<CurrencyRate> oldCurrencyRates;
    private ArrayList<CurrencyRate> newCurrencyRates;

    public ConverterDiffCallback(ArrayList<CurrencyRate> newCurrencyRates, ArrayList<CurrencyRate> oldCurrencyRates) {
        this.newCurrencyRates = newCurrencyRates;
        this.oldCurrencyRates = oldCurrencyRates;
    }

    @Override
    public int getOldListSize() {
        return oldCurrencyRates.size();
    }

    @Override
    public int getNewListSize() {
        return newCurrencyRates.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldCurrencyRates.get(oldItemPosition).getCurrencyRate() == newCurrencyRates.get(newItemPosition).getCurrencyRate();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldCurrencyRates.get(oldItemPosition).equals(newCurrencyRates.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
