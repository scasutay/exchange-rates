package com.revolut.rates.ui.converter.adapter

import android.os.Handler
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.revolut.rates.R
import com.revolut.rates.rest.model.CurrencyRate
import com.revolut.rates.util.FormatUtil
import kotlinx.android.synthetic.main.item_converter.view.*

/**
 * Created by servetcanasutay on 06/10/2018
 */
class ConverterAdapter : RecyclerView.Adapter<ConverterAdapter.RateViewHolder>() {

    private var dataChangeListener: DataChangeListener? = null
    private var currencyRates = ArrayList<CurrencyRate>()

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return currencyRates[position].currencyCode.toLong(36)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder {
        return RateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_converter, parent, false))
    }

    override fun getItemCount(): Int {
        return currencyRates.size
    }

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
        holder.bind(currencyRates[position])
    }

    fun setDataChangeListener(dataChangeListener: DataChangeListener) {
        this.dataChangeListener = dataChangeListener
    }

    inner class RateViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val llRoot = view.item_converter_root_ll
        private val ivFlag = view.item_converter_iv
        private val tvCurrency = view.item_converter_currency_tv
        private val etAmount = view.item_converter_amount_et

        fun bind(currencyRate: CurrencyRate) {
            ivFlag.countryCode = currencyRate.currencyCode
            tvCurrency.text = currencyRate.currencyCode

            etAmount.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    return@OnFocusChangeListener
                }

                layoutPosition.takeIf { it > 0 }?.also { currentPosition ->
                    currencyRates.removeAt(currentPosition).also {
                        currencyRates.add(0, it)
                    }
                    notifyItemMoved(currentPosition, 0)
                    Handler().post {
                        dataChangeListener?.onDataChanged(currencyRates)
                    }
                }
            }

            etAmount.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (etAmount.isFocused && !text.isNullOrEmpty()) {
                        currencyRates[0].currencyRate = FormatUtil().currencyFormat(itemView.context).parse(text.toString()).toDouble()
                        dataChangeListener?.onDataChanged(currencyRates)
                    }
                }
            })

            llRoot.setOnClickListener { etAmount.requestFocus() }

            if (!etAmount.isFocused)
                etAmount.setText(FormatUtil().currencyFormat(itemView.context).format(currencyRate.currencyRate))
        }
    }

    fun updateRates(newCurrencyRates: ArrayList<CurrencyRate>) {
        val diffResult = DiffUtil.calculateDiff(ConverterDiffCallback(currencyRates, newCurrencyRates))
        currencyRates = newCurrencyRates.also {
            diffResult.dispatchUpdatesTo(this)
        }
    }

    interface DataChangeListener {
        fun onDataChanged(currencyRates: ArrayList<CurrencyRate>)
    }
}