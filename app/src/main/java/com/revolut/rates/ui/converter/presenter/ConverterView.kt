package com.revolut.rates.ui.converter.presenter

import com.revolut.rates.base.mvp.BaseView
import com.revolut.rates.rest.model.CurrencyRate

/**
 * Created by servetcanasutay on 02/10/2018
 */
interface ConverterView: BaseView {

    fun onCurrenciesFetched(rateList: ArrayList<CurrencyRate>)
}