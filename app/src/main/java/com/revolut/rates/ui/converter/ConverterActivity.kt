package com.revolut.rates.ui.converter

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.inputmethod.InputMethodManager
import com.revolut.rates.R
import com.revolut.rates.base.BaseActivity
import com.revolut.rates.rest.model.CurrencyRate
import com.revolut.rates.ui.converter.adapter.ConverterAdapter
import com.revolut.rates.ui.converter.di.ConverterActivityModule
import com.revolut.rates.ui.converter.di.DaggerConverterActivityComponent
import com.revolut.rates.ui.converter.presenter.ConverterPresenter
import com.revolut.rates.ui.converter.presenter.ConverterView
import kotlinx.android.synthetic.main.activity_converter.*
import javax.inject.Inject

/**
 * Created by servetcanasutay on 02/10/2018
 */
class ConverterActivity : BaseActivity(), ConverterView, ConverterAdapter.DataChangeListener {
    @Inject
    internal lateinit var presenter: ConverterPresenter

    @Inject
    internal lateinit var converterAdapter: ConverterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_converter)

        initViews()

        presenter.getRates(ArrayList())
    }

    override fun onActivityInject() {
        DaggerConverterActivityComponent.builder().appComponent(getAppcomponent())
                .converterActivityModule(ConverterActivityModule(this))
                .build()
                .inject(this)
        presenter.attachView(this)
    }

    override fun onCurrenciesFetched(rateList: ArrayList<CurrencyRate>) {
        recyclerView.layoutManager.onRestoreInstanceState(recyclerView.layoutManager.onSaveInstanceState())
        converterAdapter.updateRates(rateList)
    }

    override fun onDataChanged(currencyRates: ArrayList<CurrencyRate>) {
        presenter.getRates(currencyRates)
    }

    private fun initViews() {
        converterAdapter.setDataChangeListener(this)
        with(recyclerView) {
            layoutManager = LinearLayoutManager(this@ConverterActivity)
            setOnTouchListener { v, _ ->
                (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        .apply { hideSoftInputFromWindow(v.windowToken, 0) }
                false
            }
            recyclerView.adapter = converterAdapter
        }
    }
}