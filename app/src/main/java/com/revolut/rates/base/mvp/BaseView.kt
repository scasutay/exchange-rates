package com.revolut.rates.base.mvp

/**
 * Created by servetcanasutay on 02/10/2018
 */

interface BaseView {
    fun onError()
    fun setPresenter(presenter: BasePresenter<*>)
}