package com.revolut.rates.base.di

import javax.inject.Scope

/**
 * Created by servetcanasutay on 02/10/2018
 */

@Scope
@Retention annotation class ActivityScope