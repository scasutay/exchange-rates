package com.revolut.rates.base.di.component

import android.app.Application
import android.content.res.Resources
import com.google.gson.Gson
import com.revolut.rates.base.di.module.ApiModule
import com.revolut.rates.base.di.module.AppModule
import com.revolut.rates.base.di.module.OkHttpModule
import com.revolut.rates.base.di.module.RetrofitModule
import com.revolut.rates.base.util.AppSchedulerProvider
import com.revolut.rates.rest.AppService
import dagger.Component
import io.reactivex.disposables.CompositeDisposable
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by servetcanasutay on 02/10/2018
 */

@Singleton
@Component(modules = arrayOf(AppModule::class, RetrofitModule::class, ApiModule::class, OkHttpModule::class))
interface AppComponent {
    fun application(): Application
    fun gson(): Gson
    fun resources(): Resources
    fun retrofit(): Retrofit
    fun endpoints(): AppService
    fun cache(): Cache
    fun client(): OkHttpClient
    fun loggingInterceptor(): HttpLoggingInterceptor
    fun compositeDisposable(): CompositeDisposable
    fun schedulerProvider(): AppSchedulerProvider
}