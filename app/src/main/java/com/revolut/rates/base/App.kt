package com.revolut.rates.base

import android.app.Application
import com.revolut.rates.base.di.component.AppComponent
import com.revolut.rates.base.di.component.DaggerAppComponent
import com.revolut.rates.base.di.module.AppModule

/**
 * Created by servetcanasutay on 02/10/2018
 */
class App: Application() {

    companion object{
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

}