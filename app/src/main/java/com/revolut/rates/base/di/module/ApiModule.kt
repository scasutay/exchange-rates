package com.revolut.rates.base.di.module

import com.revolut.rates.rest.AppService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by servetcanasutay on 02/10/2018
 */

@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesEndpoints(retrofit: Retrofit): AppService = retrofit.create(AppService::class.java)
}