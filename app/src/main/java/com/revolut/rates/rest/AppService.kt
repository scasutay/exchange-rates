package com.revolut.rates.rest

import com.revolut.rates.rest.model.RateResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by servetcanasutay on 02/10/2018
 */
interface AppService {

    @GET("latest")
    fun getLatest(@Query("base") key: String): Observable<RateResponse>
}