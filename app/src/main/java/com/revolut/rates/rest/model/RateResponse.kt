package com.revolut.rates.rest.model

/**
 * Created by servetcanasutay on 02/10/2018
 */

data class RateResponse(val base: String, val date: String, val rates: MutableMap<String, Double>)